/**
 * @file   cohesive_law_viscoelastic_inline.cc
 * @author Thibault Roch <thibault.roch@epfl.ch>
 * @date   Wed Nov 18 18:56:06 2020
 *
 * @brief  Implementation of the inline functions of the CohesiveLawViscoelastic
 * class
 *
 * @section LICENSE
 *
 * cRacklet - A spectral boundary integral method for interface fracture
 * simulation Copyright (©) 2012 - 2013 Fabian Barras 2014 EPFL (Ecole
 * Polytechnique Fédérale de Lausanne) Laboratory (LSMS - Laboratoire de
 * Simulation en Mécanique des Solides)
 *
 * cRacklet is the result of a collaboration between the Computational Solid
 * Mechanics Laboratory (LSMS) of Ecole Polytechnique Fédérale de Lausanne
 * (EPFL), Switzerland and the Department of Aerospace Engineering of the
 * University of Illinois at Urbana-Champaign, United States of America.
 *
 * cRacklet is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * cRacklet is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
/* -------------------------------------------------------------------------- */
#include <math.h>

#include <algorithm>
#include <cmath>
#include <complex>
#include <limits>
#include <vector>

#include "cRacklet_common.hh"
void CohesiveLawViscoelastic::computeIndepNormalVelocities(UInt ix, UInt iz) {
  std::vector<Real> temp_veloc(2);
  std::vector<Real> cmpted_stress(2);
  Real delta_overlap;

  UInt i = ix + iz * n_ele[0];

  for (UInt side = 0; side < 2; ++side) {
    cmpted_stress[side] = (*stresses[side])[i * dim + 1];
  }

  Real rate = (*velocities[0])[i * dim + 1] - (*velocities[1])[i * dim + 1];
  Real new_rate = 0.;
  temp_veloc[0] = (*velocities[0])[i * dim + 1];
  temp_veloc[1] = (*velocities[1])[i * dim + 1];

  // temp_veloc[0] = cs[0]/(mu[0]*eta[0]) * (cmpted_stress[0]-nor_strength[i]);
  // temp_veloc[1] = cs[1]/(mu[1]*eta[1]) * (nor_strength[i] -
  // cmpted_stress[1]); rate = temp_veloc[0] - temp_veloc[1];

  Real converg_v = 1.0;
  Real converg_s = 1.0;
  Real gamma = 0.1;
  Real delta_v_0 = 0.0;
  Real delta_v_1 = 0.0;
  Real delta_s_0, delta_s_1, K;
  Real m = 0;
  Real local_strength, tangent;
  Real new_strength = 0.;
  Real max_rate = std::numeric_limits<Real>::max();
  if (formulation->isLimiting()) {
    max_rate = lim_velocity[i];
  }
  // partial derivatives of objective functions
  Real df0_dv0, df1_dv1;
  Real det_J;
  Real alpha_0, alpha_1;

  // multivariate Newton method to compute velocities
  while ((!cRacklet::has_converged(converg_v))) {
    // Compute the strength

    local_strength = formulation->getStrength(
        max_nor_strength[i] * (1 - this->op_eq[i]), rate, lim_velocity[i]);

    tangent = formulation->getTangent(
        max_nor_strength[i] * (1 - this->op_eq[i]), rate, lim_velocity[i]);

    alpha_0 = mu[0] * eta[0] / cs[0];
    alpha_1 = mu[1] * eta[1] / cs[1];
    df0_dv0 = -tangent - mu[0] * eta[0] / cs[0];
    df1_dv1 = -tangent - mu[1] * eta[1] / cs[1];
    // det_J = df0_dv0 * df1_dv1 - tangent * tangent;
    det_J = tangent * (alpha_0 + alpha_1) + alpha_0 * alpha_1;

    // delta_s_0 = cmpted_stress[0] - local_strength -
    //             mu[0] * eta[0] * temp_veloc[0] / cs[0];
    delta_s_0 = cmpted_stress[0] - local_strength - temp_veloc[0] * alpha_0;
    // delta_s_1 = local_strength - cmpted_stress[1] -
    //            mu[1] * eta[1] * temp_veloc[1] / cs[1];
    delta_s_1 = local_strength - cmpted_stress[1] - temp_veloc[1] * alpha_1;

    // delta_v_0 = df1_dv1 * delta_s_0 - tangent * delta_s_1;
    delta_v_0 = -tangent * (cmpted_stress[0] - cmpted_stress[1] -
                            temp_veloc[0] * alpha_0 - temp_veloc[1] * alpha_1) -
                alpha_1 * (delta_s_0);
    delta_v_0 /= det_J;
    // delta_v_1 = df0_dv0 * delta_s_1 - tangent * delta_s_0;
    delta_v_1 = -tangent * (cmpted_stress[0] - cmpted_stress[1] -
                            temp_veloc[0] * alpha_0 - temp_veloc[1] * alpha_1) -
                alpha_0 * (delta_s_1);
    delta_v_1 /= det_J;

    new_rate = (temp_veloc[0] - delta_v_0) - (temp_veloc[1] - delta_v_1);
    new_strength = formulation->getStrength(
        max_nor_strength[i] * (1 - this->op_eq[i]), new_rate, lim_velocity[i]);
    while (new_strength > 2. * local_strength || new_rate >= max_rate) {
      delta_v_0 /= 2.;
      delta_v_1 /= 2.;
      new_rate = (temp_veloc[0] - delta_v_0) - (temp_veloc[1] - delta_v_1);
      new_strength =
          formulation->getStrength(max_nor_strength[i] * (1 - this->op_eq[i]),
                                   new_rate, lim_velocity[i]);
    }

    temp_veloc[0] -= delta_v_0;
    temp_veloc[1] -= delta_v_1;

    rate = temp_veloc[0] - temp_veloc[1];

    // Here we add a fictional minimum for the rate as being 1 to avoid problems
    // when the rate is equal to 0
    converg_v =
        std::max(std::abs(delta_v_0), std::abs(delta_v_1)) / std::max(1., rate);

    m += 1;

    if (m > 5000) {
      std::cout << "Strength : " << local_strength << std::endl;
      std::cout << "Tangent : " << tangent << std::endl;
      std::cout << "Rate : " << rate << std::endl;
      std::cout << "op_eq : " << this->op_eq[i] << std::endl;
      std::cout << "Normal..." << std::endl;
      std::cout << "Delta v : " << delta_v_0 << ", " << delta_v_1 << std::endl;
      std::cout << "Delta s : " << delta_s_0 << ", " << delta_s_1 << std::endl;
      std::cout << "Error v : " << converg_v << std::endl;
      std::cout << "Error s : " << converg_s << std::endl;
      cRacklet::error("Solution has not been found");
    }
  }

  local_strength = formulation->getStrength(
      max_nor_strength[i] * (1 - this->op_eq[i]), rate, lim_velocity[i]);

  Real dt = dxmin * beta / (std::max(cs[0], cs[1]));

  delta_overlap = (*displacements[0])[i * dim + 1] -
                  (*displacements[1])[i * dim + 1] + dt * rate;

  if (cRacklet::is_negative(delta_overlap) && (!allow_overlapping)) {
    computeContactVelocities(ix, iz);
  } else {
    this->prev_nor_vel[i] = rate;

    (*velocities[0])[i * dim + 1] = temp_veloc[0];
    (*velocities[1])[i * dim + 1] = temp_veloc[1];
    (*intfc_trac)[i * dim + 1] = local_strength;
    computeShearVelocities(shr_strength[i], i);
  }
}

/* -------------------------------------------------------------------------- */
void CohesiveLawViscoelastic::computeContactVelocities(UInt ix, UInt iz) {
  Real aux;
  Real temp_velot;
  Real temp_trac;
  Real strength;
  std::vector<Real> cmpted_stress(2);

  UInt i = ix + iz * n_ele[0];

  for (UInt side = 0; side < 2; ++side) {
    cmpted_stress[side] = (*stresses[side])[i * dim + 1];
  }

  aux = ((*displacements[0])[i * dim + 1] - (*displacements[1])[i * dim + 1]) /
        (dxmin * beta);
  temp_velot = 1 / (eta[0] / cs[0] + eta[1] / cs[1] / (mu[0] / mu[1])) *
               ((cmpted_stress[0] - cmpted_stress[1]) / mu[0] -
                aux * eta[1] / cs[1] / (mu[0] / mu[1]));
  (*velocities[0])[i * dim + 1] = temp_velot;
  (*velocities[1])[i * dim + 1] = temp_velot + aux;

  temp_trac = cmpted_stress[0] - eta[0] * mu[0] * temp_velot / cs[0];
  (*intfc_trac)[i * dim + 1] = temp_trac;

  contact_law->computeFricStrength(temp_trac, strength, i, it);

  computeShearVelocities(strength, i);

  ind_crack[i] = 3;
  fric_strength[i] = strength;
}

/* -------------------------------------------------------------------------- */
void CohesiveLawViscoelastic::computeShearVelocities(Real strength, UInt i) {
  std::vector<Real> trac(2);
  Real shr_trac;

  for (UInt j = 0; j < 2; ++j) {
    trac[j] = (*stresses[0])[i * dim + 2 * j] -
              mu[0] * (*velocities[0])[i * dim + 2 * j] / cs[0];
  }

  shr_trac = sqrt((trac[0] * trac[0]) + (trac[1] * trac[1]));

  if ((strength < shr_trac) || (strength == 0))
    computeIndepShearVelocities(strength, i);
  else {
    for (UInt j = 0; j < (dim - 1); ++j) {
      (*intfc_trac)[i * dim + 2 * j] = trac[j];
    }
  }
}

/* -------------------------------------------------------------------------- */
void CohesiveLawViscoelastic::computeIndepShearVelocities(Real strength,
                                                          UInt i) {
  std::vector<Real> cmpted_stress(4);
  std::vector<Real> dyn_stress(2);
  std::vector<Real> shr_veloc(2);
  Real shr_trac;

  for (UInt side = 0; side < 2; ++side) {
    for (UInt j = 0; j < 2; ++j) {
      cmpted_stress[2 * side + j] = (*stresses[side])[i * dim + 2 * j];
    }
    dyn_stress[side] =
        sqrt(cmpted_stress[2 * side] * cmpted_stress[2 * side] +
             cmpted_stress[2 * side + 1] * cmpted_stress[2 * side + 1]);
  }

  Real rate = this->prev_shr_vel[i];

  Real local_strength = formulation->getStrength(
      max_shr_strength[i] * (1 - this->op_eq[i]), rate, lim_velocity[i]);

  if (dyn_stress[0] == 0) {
    for (UInt j = 0; j < 2; ++j) {
      (*intfc_trac)[i * dim + 2 * j] = 0;
      (*velocities[0])[i * dim + 2 * j] = 0;
      (*velocities[1])[i * dim + 2 * j] = 0;
    }
  } else {
    for (UInt side = 0; side < 2; ++side) {
      Real vel_0 = (*velocities[side])[i * dim];
      Real vel_1 = (*velocities[side])[i * dim + 2];
      shr_veloc[side] = sqrt(vel_0 * vel_0 + vel_1 * vel_1);
    }

    Real converg_v = 1.0;
    Real converg_s = 1.0;
    Real gamma = 0.1;
    Real delta_v_0 = 0.0;
    Real delta_v_1 = 0.0;
    Real delta_s_0, delta_s_1, K;
    Real m = 0;
    Real tangent;
    Real new_strength = 0.;
    Real new_rate = 0.;
    Real max_rate = std::numeric_limits<Real>::max();
    if (formulation->isLimiting()) {
      max_rate = lim_velocity[i];
    }
    // partial derivatives of objective functions
    Real df0_dv0, df1_dv1;
    Real det_J;
    Real alpha_0, alpha_1;

    // multivariate Newton method to compute velocities
    while ((!cRacklet::has_converged(converg_v))) {  //&&(is_good)) {

      // Compute the strength

      local_strength = formulation->getStrength(
          max_shr_strength[i] * (1 - op_eq[i]), rate, lim_velocity[i]);

      tangent = formulation->getTangent(max_shr_strength[i] * (1 - op_eq[i]),
                                        rate, lim_velocity[i]);

//       df0_dv0 = -tangent - mu[0] / cs[0];
//       df1_dv1 = -tangent - mu[1] / cs[1];
//       det_J = df0_dv0 * df1_dv1 - tangent * tangent;
// 
//       delta_s_0 = dyn_stress[0] - local_strength - mu[0] * shr_veloc[0] / cs[0];
//       delta_s_1 = local_strength - dyn_stress[1] - mu[1] * shr_veloc[1] / cs[1];
// 
//       delta_v_0 = df1_dv1 * delta_s_0 - tangent * delta_s_1;
//       delta_v_0 /= det_J;
//       delta_v_1 = df0_dv0 * delta_s_1 - tangent * delta_s_0;
//       delta_v_1 /= det_J;
// 
//       shr_veloc[0] -= delta_v_0;
//       shr_veloc[1] -= delta_v_1;
// 
//       rate = shr_veloc[0] - shr_veloc[1];

       //
       // Try to avoid floating point error and overshoot
       //
       alpha_0 = mu[0] / cs[0];
       alpha_1 = mu[1] / cs[1];
       df0_dv0 = -tangent - mu[0] / cs[0];
       df1_dv1 = -tangent - mu[1] / cs[1];
       // det_J = df0_dv0 * df1_dv1 - tangent * tangent;
       det_J = tangent * (alpha_0 + alpha_1) + alpha_0 * alpha_1;
 
       // delta_s_0 = cmpted_stress[0] - local_strength -
       //             mu[0] * eta[0] * temp_veloc[0] / cs[0];
       delta_s_0 = dyn_stress[0] - local_strength - shr_veloc[0] * alpha_0;
       // delta_s_1 = local_strength - cmpted_stress[1] -
       //            mu[1] * eta[1] * temp_veloc[1] / cs[1];
       delta_s_1 = local_strength - dyn_stress[1] - shr_veloc[1] * alpha_1;
 
       // delta_v_0 = df1_dv1 * delta_s_0 - tangent * delta_s_1;
       delta_v_0 = -tangent * (dyn_stress[0] - dyn_stress[1] -
                               shr_veloc[0] * alpha_0 - shr_veloc[1] * alpha_1) -
                   alpha_1 * (delta_s_0);
       delta_v_0 /= det_J;
       // delta_v_1 = df0_dv0 * delta_s_1 - tangent * delta_s_0;
       delta_v_1 = -tangent * (dyn_stress[0] - dyn_stress[1] -
                               shr_veloc[0] * alpha_0 - shr_veloc[1] * alpha_1) -
                   alpha_0 * (delta_s_1);
       delta_v_1 /= det_J;
 
       new_rate = (shr_veloc[0] - delta_v_0) - (shr_veloc[1] - delta_v_1);
       new_strength =
           formulation->getStrength(max_shr_strength[i] * (1 - this->op_eq[i]),
                                    new_rate, lim_velocity[i]);
       while (new_strength > 2. * local_strength || new_rate >= max_rate) {
         delta_v_0 /= 2.;
         delta_v_1 /= 2.;
         new_rate = (shr_veloc[0] - delta_v_0) - (shr_veloc[1] - delta_v_1);
         new_strength =
             formulation->getStrength(max_shr_strength[i] * (1 - this->op_eq[i]),
                                      new_rate, lim_velocity[i]);
       }
 
       shr_veloc[0] -= delta_v_0;
       shr_veloc[1] -= delta_v_1;
 
       rate = shr_veloc[0] - shr_veloc[1];
       //
       // End of alternative part
       //

      // Here we add a fictional minimum for the rate as being 1 to avoid
      // problems when the rate is equal to 0
      converg_v = std::max(std::abs(delta_v_0), std::abs(delta_v_1)) /
                  std::max(1., rate);

      m += 1;

      if (m > 500) {
        std::cout << "Shear..." << std::endl;
        std::cout << "Delta v : " << delta_v_0 << ", " << delta_v_1
                  << std::endl;
        std::cout << "Delta s : " << delta_s_0 << ", " << delta_s_1
                  << std::endl;
        std::cout << "Error v : " << converg_v << std::endl;
        std::cout << "Error s : " << converg_s << std::endl;
        cRacklet::error("Solution has not been found");
      }
    }

    for (UInt j = 0; j < 2; ++j) {
      (*velocities[0])[i * dim + 2 * j] =
          shr_veloc[0] * cmpted_stress[j] / dyn_stress[0];
      (*velocities[1])[i * dim + 2 * j] =
          shr_veloc[1] * cmpted_stress[2 + j] / dyn_stress[1];
      (*intfc_trac)[i * dim + 2 * j] =
          local_strength * cmpted_stress[j] / dyn_stress[0];
    }
  }
  this->prev_shr_vel[i] = rate;
}
