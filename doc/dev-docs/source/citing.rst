Citing
======

To give proper credit to cRacklet and the researchers who have developed it, please cite cRacklet as:
Roch et al., (2022). cRacklet: a spectral boundary integral method library for interfacial rupture simulation. Journal of Open Source Software, 7(69), 3724, https://doi.org/10.21105/joss.03724
