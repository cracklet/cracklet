/* -------------------------------------------------------------------------- */
#include <stdio.h>

#include <iostream>
#include <string>
#include <vector>

#include "cohesive_law_viscoelastic.hh"
#include "coulomb_law.hh"
#include "data_dumper.hh"
#include "interfacer.hh"
#include "regularized_coulomb_law.hh"
#include "simulation_driver.hh"
#include "spectral_model.hh"

/* -------------------------------------------------------------------------- */

int main(int argc, char* argv[]) {
  std::string sim_name = "Viscoelastic 2D";

  std::string input_file = "input_viscoelastic.cra";

  DataRegister::readInputFile(input_file);

  // Geometry description
  Real nu = DataRegister::getParameter<Real>("nu");
  Real E = DataRegister::getParameter<Real>("E");
  Real cs = DataRegister::getParameter<Real>("cs");

  // Cut of the loaded material kernels
  UInt tcut = DataRegister::getParameter<UInt>("tcut");

  // Loading case
  Real psi = 45;
  Real phi = 45;

  // Cohesive parameters
  Real crit_n_open =
      DataRegister::getParameter<Real>("critical_normal_opening");
  Real crit_s_open = DataRegister::getParameter<Real>("critical_shear_opening");
  Real max_s_str = DataRegister::getParameter<Real>("max_normal_strength");
  Real max_n_str = DataRegister::getParameter<Real>("max_shear_strength");
  Real lim_velocity = DataRegister::getParameter<Real>("lim_velocity");

  Real load = DataRegister::getParameter<Real>("load");

  // Real G_length = crit_n_open*max_n_str/(load*load*M_PI)*E/(1-nu*nu);

  Real dom_size = DataRegister::getParameter<Real>("dom_size");

  std::cout << "Summary of the current simulation: " << std::endl
            << "Domain size: " << dom_size << std::endl;

  UInt nb_time_steps = 0;
  UInt nb_elements = DataRegister::getParameter<UInt>("nb_elements");
  Real dx = dom_size / nb_elements;
  Real propagation_domain = 0.75 * dom_size;

  // Friction paramters
  Real regularized_time_scale = 0.1;
  Real coef_frict = 0.25;
  std::shared_ptr<ContactLaw> contactlaw =
      std::make_shared<RegularizedCoulombLaw>(
          coef_frict, regularized_time_scale, nb_elements);

  std::cout << "Input parameters summary : " << std::endl
            << "Loading : " << load << std::endl;

  SpectralModel model(nb_elements, nb_time_steps, dom_size, nu, E, cs, tcut,
                      sim_name);

  Real beta = 0.1;  // Stable time step coefficient

  model.initModel(beta);
  model.setLoadingCase(0, psi, phi);

  Interfacer<_viscoelastic_coupled_cohesive> interfacer(model);
  // Interfacer<_linear_coupled_cohesive> interfacer(model);

  interfacer.createUniformInterface();

  CohesiveLawViscoelastic& cohesive_law =
      dynamic_cast<CohesiveLawViscoelastic&>((model.getInterfaceLaw()));
  // CohesiveLaw& cohesive_law =
  // dynamic_cast<CohesiveLaw&>((model.getInterfaceLaw()));
  cohesive_law.preventSurfaceOverlapping(contactlaw);

  cohesive_law.initLinearFormulation();

  interfacer.createThroughCrack(0, 2 * dx);
  interfacer.createThroughWall(propagation_domain, dom_size);

  model.updateLoads();
  model.initInterfaceFields();

  const CrackProfile* t_displacements = model.readData(_top_displacements);
  const CrackProfile* b_displacements = model.readData(_bottom_displacements);
  const CrackProfile* intfc_trac = model.readData(_interface_tractions);

  DataDumper dumper(model);

  dumper.initDumper("ST_Diagram_id.cra", _id_crack);

  UInt chkpt = 3;
  UInt wdth = 15;

  UInt x_tip = 0;
  UInt t = 0;

  UInt t_max = 10000;

  while (t < t_max) {
    UInt top = 250000;

    UInt load_factor = std::min(84 * t, top);

    model.setLoadingCase(load_factor * load, psi, phi);
    model.updateLoads();

    model.updateDisplacements();
    model.fftOnDisplacements();
    model.computeStress();
    model.computeInterfaceFields();
    model.increaseTimeStep();

    x_tip = model.getCrackTipPosition(0., nb_elements);

    if (t % 100 == 0) {
      std::cout << "Crack position at " << x_tip * (Real)(dx) << " over "
                << propagation_domain << std::endl;

      std::cout << std::setw(wdth)
                << (*t_displacements)[chkpt * 2 + 1] -
                       (*b_displacements)[chkpt * 2 + 1]
                << std::setw(wdth)
                << (*t_displacements)[chkpt * 2] - (*b_displacements)[chkpt * 2]
                << std::setw(wdth) << (*intfc_trac)[chkpt * 2 + 1]
                << std::setw(wdth) << (*intfc_trac)[chkpt * 2]
                << std::setw(wdth) << std::endl;

      dumper.dumpAll();
    }

    ++t;
  }

  return 0;
}
